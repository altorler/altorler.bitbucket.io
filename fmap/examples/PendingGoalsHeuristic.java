/* 
 * Copyright (C) 2017 Universitat Politècnica de València
 *
 * This file is part of FMAP.
 *
 * FMAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FMAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FMAP. If not, see <http://www.gnu.org/licenses/>.
 */

package org.agreement_technologies.service.map_heuristic;

import java.util.ArrayList;
import java.util.BitSet;
import org.agreement_technologies.common.map_heuristic.HPlan;
import org.agreement_technologies.common.map_heuristic.Heuristic;
import org.agreement_technologies.common.map_planner.Condition;
import org.agreement_technologies.common.map_planner.PlannerFactory;
import org.agreement_technologies.service.map_planner.PlannerFactoryImp;

/**
 * Simple heuristic that evaluates the quality of a plan according to the number of pending goals
 * 
 * @author Alejandro Torreño
 * @version %I%, %G%
 * @since 1.0
 */
public class PendingGoalsHeuristic implements Heuristic {
    private final int globalVars;

    
    public PendingGoalsHeuristic(PlannerFactory pf) {
        PlannerFactoryImp pfi = (PlannerFactoryImp) pf;
        this.globalVars = pfi.getNumGlobalVariables();
    }

    /**
     * Heuristic evaluation of a plan. The resulting value is stored inside the
     * plan (see setH method in Plan interface).
     *
     * @param p Plan to evaluate
     * @param threadIndex Thread index, for multi-threading purposes
     * @since 1.0
     */
    @Override
    public void evaluatePlan(HPlan p, int threadIndex) {
        int[] frontierState = p.computeCodeState(p.linearization(), globalVars);
        int h = p.getFinalStep().getPrecs().length;
        
        //Check achieved goals
        for(Condition pre: p.getFinalStep().getPrecs()) {
            if(frontierState[pre.getVarCode()] == pre.getValueCode()) {
                h--;
            }
        }
        //Set number of pending goals as h value of plan p
        p.setH(h, 0);
        return;
    }

    /**
     * Heuristically evaluates the cost of reaching the agent's private goals.
     *
     * @param p Plan to evaluate
     * @param threadIndex Thread index, for multi-threading purposes
     * @since 1.0
     */
    @Override
    public void evaluatePlanPrivacy(HPlan p, int threadIndex) {
    }

    /**
     * Synchronization step after the distributed heuristic evaluation.
     *
     * @since 1.0
     */
    @Override
    public void waitEndEvaluation() {
    }
    
    /**
     * Begining of the heuristic evaluation stage.
     *
     * @param basePlan Base plan, whose successors will be evaluated
     * @since 1.0
     */
    @Override
    public void startEvaluation(HPlan basePlan) {
    }

    /* Gets information about a given topic.
     *
     * @param infoFlag Topic to get information about. For this heuristic, no
     * topics are defined
     * @return <code>null</code>, as no information is available to retrieve
     * @since 1.0
     */
    @Override
    public Object getInformation(int infoFlag) {
        return null;
    }

    /**
     * Checks if the current heuristic evaluator supports multi-threading.
     *
     * @return <code>true</code>, if multi-therading evaluation is available.
     * <code>false</code>, otherwise
     * @since 1.0
     */
    @Override
    public boolean supportsMultiThreading() {
        return false;
    }

    /**
     * Checks if the current heuristic evaluator requieres an additional stage
     * for landmarks evaluation.
     *
     * @return <code>true</code>, if a landmarks evaluation stage is required.
     * <code>false</code>, otherwise
     * @since 1.0
     */
    @Override
    public boolean requiresHLandStage() {
        return false;
    }
    
    /**
     * Multi-heuristic evaluation of a plan, also evaluates the remaining
     * landmarks to achieve. The resulting value is stored inside the plan (see
     * setH method in Plan interface).
     *
     * @param p Plan to evaluate
     * @param threadIndex Thread index, for multi-threading purposes
     * @param achievedLandmarks List of already achieved landmarks
     * @since 1.0
     */
    @Override
    public void evaluatePlan(HPlan p, int threadIndex, ArrayList<Integer> achievedLandmarks) {
    }

    /**
     * Returns the total number of global (public) landmarks.
     *
     * @return Total number of global (public) landmarks
     * @since 1.0
     */
    @Override
    public int numGlobalLandmarks() {
        return 0;
    }

    /**
     * Returns the new landmarks achieved in this plan.
     *
     * @param plan Plan to check
     * @param achievedLandmarks Already achieved landmarks
     * @return List of indexes of the new achieved landmarks
     * @since 1.0
     */
    @Override
    public ArrayList<Integer> checkNewLandmarks(HPlan plan, BitSet achievedLandmarks) {
        return null;
    }

}
